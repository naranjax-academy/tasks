const number = 5;
for (let multiplier = 1; (multiplier <= 10); multiplier++) {
  console.log(`${number} x ${multiplier} = ${number * multiplier}`);
}
